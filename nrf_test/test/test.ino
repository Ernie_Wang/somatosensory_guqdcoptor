
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"




//radio variables
float Data[5];

RF24 radio(9,10);

const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };



//    Declaring mpu_6050 global variables
MPU6050 mpu;

// Timers
unsigned long timer1 = 0, timer2;;
float timeStep = 0.075;

// Pitch, Roll and Yaw values
float pitch = 0;
float roll = 0;
float yaw = 0;


//bottoms
bool blackpin=A1, redpin=A2;

//fnction name

void gyro_cal();

void setup(void)
{
    
    Serial.begin(57600);
    
    ////bottom set
    pinMode(redpin, INPUT_PULLUP);
    pinMode(blackpin, INPUT_PULLUP);
    
    ////MPU6050 set
    ////-----set up gyro
    while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))
    {
        Serial.println("Could not find a valid MPU6050 sensor, check wiring!");
        delay(500);
    }
    
    // Calibrate gyroscope. The calibration must be at rest.
    // If you don't want calibrate, comment this line.
    mpu.calibrateGyro();
    
    // Set threshold sensivty. Default 3.
    // If you don't want use threshold, comment this line or set 0.
    mpu.setThreshold(3);
    
    
    
    
    ////radio set
    radio.begin();
    radio.setRetries(15,15);
    
    radio.openWritingPipe(pipes[0]);
    radio.openReadingPipe(1,pipes[1]);
    radio.stopListening();
    radio.setPALevel(RF24_PA_MIN);
    radio.setDataRate(RF24_1MBPS);
    Serial.println("ready");
    // delay(5000);
    
    
    
    
}

void loop(void)
{
    timer2=millis();
    timeStep=(timer2-timer1)*0.001;
    timer1 =millis();
    
    //get data
    gyro_cal();
    digitalRead(redpin);
    digitalRead(blackpin);
    
    //setup Data to send
    Data[0]=redpin;
    Data[1]=blackpin;
    Data[2]=pitch;
    Data[3]=roll;
    Data[4]=yaw;
    
    radio.write(&Data , sizeof(Data));
    Serial.print("sending：");
    Serial.print(Data[0]);Serial.print("  ");
    Serial.print(Data[1]);Serial.print("  ");
    Serial.print(Data[2]);Serial.print("  ");
    Serial.print(Data[3]);Serial.print("  ");
    Serial.print(Data[4]);Serial.println("  ");
    
    /*unsigned long time = millis();
     Serial.print("Now sending");
     Serial.print(time);
     bool ok = radio.write( &time, sizeof(unsigned long) );
     if (ok)
     Serial.println(".......OK");
     else
     Serial.println(".......FAILED");*/
    //delay(4);
}



void gyro_cal() {
    
    // Read normalized values
    Vector norm = mpu.readNormalizeGyro();
    
    // Calculate Pitch, Roll and Yaw
    pitch = pitch + norm.YAxis * timeStep;
    roll = roll + norm.XAxis * timeStep;
    yaw = yaw + norm.ZAxis * timeStep;
    
    // Output raw
    Serial.print(" Pitch = ");
    Serial.print(pitch);
    Serial.print(" Roll = ");
    Serial.print(roll);
    Serial.print(" Yaw = ");
    Serial.print(yaw);
    
    
}


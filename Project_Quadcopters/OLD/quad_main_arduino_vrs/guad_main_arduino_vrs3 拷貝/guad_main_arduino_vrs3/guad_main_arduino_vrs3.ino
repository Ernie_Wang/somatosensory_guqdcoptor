// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE



//#include "I2Cdev.h"
//#include "MPU6050.h"
#include "Wire.h"
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"



////---------------MY PROGRAM START FROM HERE-------------------////


float output[] = {0, 0, 0, 0};
const int motor[] = {5, 6, 9, 3};// 1=>5  2=>6  3=>9  4=>10  support engine=>3
bool controled=false, start=false;
int throttle=0;
int countstart=0, countend=0, countdiff=0;
bool throttlezero=false;
bool starting=false;

//    Declaring mpu_6050 global variables
int gyro_x, gyro_y, gyro_z;
long acc_x, acc_y, acc_z, acc_total_vector;
int temperature;
long gyro_x_cal, gyro_y_cal, gyro_z_cal, acc_x_cal, acc_y_cal, acc_z_cal, angle_pitch_cal, angle_roll_cal, angle_yaw_cal;
long first_timer, second_timer;
float angle_pitch, angle_roll, angle_yaw, time_diff;
int angle_pitch_buffer, angle_roll_buffer, angle_yaw_buffer;
boolean set_gyro_angles;
float angle_roll_acc, angle_pitch_acc, angle_yaw_acc;
const int gyro_sensi=65.5;  //dps
const int acc_sensi=8;     //g
int reset_gyro=0;
float tmp;

//    PID global valiables


float ProllConst = 0.116;               //roll P-controller常數
float IrollConst = 0.332;              //roll I-controller常數
float DrollConst = 0.038;              //roll D-controller常數
int pid_max_roll = 10;                    //Maximum output of the PID-controller (+/-)

float PpitchConst = ProllConst;  //pitch P-controller.常數
float IpitchConst = IrollConst;  //pitch I-controller.常數
float DpitchConst = DrollConst;  //pitch D-controller.常數
int pid_max_pitch = pid_max_roll;          //Maximum output of the PID-controller (+/-)

float PyawConst = 0.116;                //yaw P-controller.常數 //4.0
float IyawConst = 0.7;               //yaw I-controller.常數 //0.02
float DyawConst = 2;                //yaw D-controller.常數
int pid_max_yaw = 60;                     //Maximum output of the PID-controller (+/-)

int acc_axis[4], gyro_axis[4];
int calTime;

double gyro_axis_cal[4];
double gyro_pitch, gyro_roll, gyro_yaw;

float roll_setpoint =0, pitch_setpoint =0; //設定PID基準點
float yaw_setpoint=0 ;

float Error;
float pid_i_mem_roll, gyro_roll_input, PIDRoll,LastDRollError;
float pid_i_mem_pitch, gyro_pitch_input, PIDPitch, LastDPitchError;
float pid_i_mem_yaw, gyro_yaw_input, PIDYaw, LastDYawError;
boolean gyro_angles_set;



//--------------------------------------------------------//

//radio globle variable
RF24 radio(7,8);

float Data[5];
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };


//-------------------------------------------------------//

//function name
void read_mpu_6050_data();
void setup_mpu_6050_registers();
void gyro_cal();
void PID_cal();

// Add setup code
void setup()
{
    //   start set up mpu_6050
    Wire.begin();             //Start I2C as master
    Serial.begin(57600);      //Use only for debugging
    pinMode(13, OUTPUT);      //Set output 13 (LED) as output
    
    
    ////-----set up gyro
    //   start set up mpu_6050
    
    setup_mpu_6050_registers();              //Setup the registers of the MPU-6050 (500dfs / +/-8g) and start the gyro
    
    digitalWrite(13, HIGH);                   //Set digital output 13 high to indicate startup
    for (int cal_int = 0; cal_int < 2000 ; cal_int ++){                  //Run this code 2000 times
        read_mpu_6050_data();                 //Read the raw acc and gyro data from the MPU-6050
        gyro_x_cal += gyro_x;                 //Add the gyro x-axis offset to the gyro_x_cal variable
        gyro_y_cal += gyro_y;                 //Add the gyro y-axis offset to the gyro_y_cal variable
        gyro_z_cal += gyro_z;                 //Add the gyro z-axis offset to the gyro_z_cal variable
        acc_x_cal+=acc_x;
        acc_y_cal+=acc_y;
        acc_z_cal+=acc_z;
        delay(3);                             //Delay 3us to simulate the 250Hz program loop
    }
    gyro_x_cal /= 2000;                       //Divide the gyro_x_cal variable by 2000 to get the avarage offset
    gyro_y_cal /= 2000;                       //Divide the gyro_y_cal variable by 2000 to get the avarage offset
    gyro_z_cal /= 2000;                       //Divide the gyro_z_cal variable by 2000 to get the avarage offset
    acc_x_cal/= 2000;
    acc_y_cal/= 2000;
    acc_z_cal/= 2000;
    
    acc_total_vector = sqrt((acc_x_cal*acc_x_cal)+(acc_y_cal*acc_y_cal)+(acc_z_cal*acc_z_cal));  //Calculate the total accelerometer vector
    //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
    angle_pitch_cal = asin((float)acc_y_cal/acc_total_vector)* 57.296;       //Calculate the pitch angle
    angle_roll_cal = asin((float)acc_x_cal/acc_total_vector)* -57.296;       //Calculate the roll angle
    digitalWrite(13, LOW);                    //All done, turn the LED off
    
    first_timer = micros();                   //Reset the loop timer
    
    Serial.print(" angle_pitch_cal = ");
    Serial.print(angle_pitch_cal);
    Serial.print(" angle_pitch_cal = ");
    Serial.print(angle_pitch_cal);
    Serial.print(" angle_pitch_cal = ");
    Serial.println(angle_pitch_cal);
    //delay(5000);
    
    ////-----set radio
    
    radio.begin();
    radio.setRetries(15,15);
    radio.openWritingPipe(pipes[1]);
    radio.openReadingPipe(1,pipes[0]);
    radio.setPALevel(RF24_PA_MIN);
    radio.setDataRate(RF24_1MBPS);
    
    //set motor
    for (int i=0; i<4; i++) {
        pinMode(motor[i], OUTPUT);
        analogWrite(motor[i], 0);
    }
    
    
    //let user Know the copter is ready
    digitalWrite(13, LOW);                    //All done, turn the LED off
    //analogWrite(motor[1], 200);
    analogWrite(motor[1], 0);
    
}

// Add loop code
void loop()
{
    read_mpu_6050_data();             //Read the raw acc and gyro data from the MPU-6050
    
    
    second_timer=micros();
    
    time_diff=second_timer-first_timer;
    time_diff=time_diff/1000000;
    
    first_timer=second_timer;
    
    
    gyro_cal();
    starting=false;
    radio.startListening();
    if (radio.available()) {
      Serial.print("get radio");
        radio.read(&Data ,sizeof(Data));
        //ok
        if (Data[0]==0&&Data[1]==0) {
            //to start
            if (throttle==0) {
                if (throttlezero==false) {        //judge whether *countstart  *countend is counting for the shut down program
                    if (countstart==0) {
                        countstart=millis();      //record the time of countstart;
                    }
                    countend=millis();            //record the time of countend;
                    countdiff=countend-countstart;
                    if (countdiff>=2500) {        //when the difference between countstart and countend is 2.5 second
                        start=true;               //you can start to control coptors
                        countstart=0;
                        countend=0;
                        throttle=1;
                        //first_timer = micros();                   //Reset the loop timer
                        //analogWrite(motor[1], 200);
                        analogWrite(motor[1], 0);
                    }
                }
                else {                            //if the countstart and countend is counting for the shut down, reset to zero
                    throttlezero=false;
                    countstart=0;
                    countend=0;
                }
                starting=true;
            }
            //emergency stop
            else {
              unsigned int tmp=millis();
              //Serial.println(tmp);
              bool stillcontrolling=false;
              unsigned int tmp2=millis();
              while(tmp2-tmp<=500) {
                radio.read(&Data ,sizeof(Data));
                /*
                Serial.print(Data[0]);Serial.print("  ");
                Serial.print(Data[1]);Serial.print("  ");
                Serial.print(Data[2]);Serial.print("  ");
                Serial.print(Data[3]);Serial.println("  ");
                */
                if(Data[0]==1||Data[1]==1) {
                  stillcontrolling=true;
                  break;
                }
                tmp2=millis();
              }
              if(!stillcontrolling) {
                if (radio.available()) {
                    radio.read(&Data ,sizeof(Data));
                    if (Data[0]==0&&Data[1]==0) {
                      throttle=0;
                      Serial.println("emerge");
                        for (int i=0; i<4; i++) {
                            analogWrite(motor[i], 0);
                        }
                        throttlezero==false;
                        start=false;
                    }
                }
              }
            }
  
        }
         Serial.print(" start "); Serial.println(start);
        
        
        
        if (start) {     //if haven't start, you won't able to start to control
            //Serial.println(" start ");
            Serial.print(" Data0 "); Serial.print(Data[0]);
            Serial.print(" Data1 "); Serial.print(Data[1]);
            Serial.print("throttle : ");
            Serial.println(throttle);
            roll_setpoint=Data[2];
            pitch_setpoint=Data[3];
            yaw_setpoint=Data[4];
             roll_setpoint=0;
        pitch_setpoint=0;
        yaw_setpoint=0;


            
            if (Data[0]==0&&Data[1]==1&&throttle<=253) {
                throttle+=2;
                //Serial.print("throttle : ");
                //Serial.println(throttle);
                throttlezero=false;
            }
            if (Data[0]==1&&Data[1]==0&&throttle>=3) {
                throttle-=2;
                //Serial.print("throttle : ");
                //Serial.println(throttle);
                throttlezero=false;
            }
           
            
            throttlezero=false;
            //remote
            
            
            //Reset the loop timer
            
            
            /*
             else {
             roll_setpoint =0;
             pitch_setpoint =0;
             }*/
            
            
            //use PID to controll static
            
            
            
        }
        
        //ok
            }
    else {
        roll_setpoint=0;
        pitch_setpoint=0;
        yaw_setpoint=0;
    }

    if (throttle<=3) {     //shut down the coptors for safety   //the logic is same as start
            if (start) {
                if (!starting) {
                    if (throttlezero==true) {
                        if (countstart==0) {
                            countstart=millis();
                        }
                        countend=millis();
                        countdiff=countend-countstart;
                        if (countdiff>=5000) {
                          
                            start=false;
                            countstart=0;
                            countend=0;
                            throttle=0;
                            Serial.println("Engine down ");
                            Serial.print("throttle : ");
                            Serial.println(throttle);
                        }
                    } else {
                        throttlezero=true;
                        countstart=0;
                        countend=0;
                    }
                }
            }
            //first_timer = micros();                   //Reset the loop timer
        }

    
    if (start) {
        PID_cal();
        //PIDYaw=PIDYaw*-1;
        //controll    throttle+pitch+roll+yaw
        if (throttle+PIDPitch+PIDRoll+PIDYaw>0&&throttle+PIDPitch+PIDRoll+PIDYaw<=255) {
            output[0] = throttle+PIDPitch+PIDRoll+PIDYaw;
        }
        else if (throttle+PIDPitch+PIDRoll+PIDYaw<=0)
            output[0]=0;
        else
            output[0]=255;
        
        if (throttle-PIDPitch+PIDRoll-PIDYaw>0&&throttle-PIDPitch+PIDRoll-PIDYaw<=255) {
            output[1]= throttle-PIDPitch+PIDRoll-PIDYaw;
        }
        else if (throttle-PIDPitch+PIDRoll-PIDYaw<=0)
            output[1]=0;
        else
            output[1]=255;
        
        if (throttle-PIDPitch-PIDRoll+PIDYaw>0&&throttle-PIDPitch-PIDRoll+PIDYaw<=255) {
            output[2]= throttle-PIDPitch-PIDRoll+PIDYaw;
        }
        else if (throttle-PIDPitch-PIDRoll+PIDYaw<=0)
            output[2]=0;
        else
            output[2]=255;
        
        if (throttle+PIDPitch-PIDRoll-PIDYaw>0&&throttle+PIDPitch-PIDRoll-PIDYaw<=255) {
            output[3]= throttle+PIDPitch-PIDRoll-PIDYaw;
        }
        else if (throttle+PIDPitch-PIDRoll-PIDYaw<=0)
            output[3]=0;
        else
            output[3]=255;
        
        
        
        analogWrite(motor[0], output[0]);
        analogWrite(motor[1], output[1]);
        analogWrite(motor[2], output[2]);
        analogWrite(motor[3], output[3]);
        //analogWrite(motor[4], throttle);
        
       
        
        Serial.print(" PIDPitch = ");
        Serial.print(PIDPitch);
        Serial.print(" PIDRoll = ");
        Serial.print(PIDRoll);
        Serial.print(" PIDYaw = ");
        Serial.print(PIDYaw);
        Serial.print(" motor1 = ");
        Serial.print(output[0]);
        Serial.print(" motor2 = ");
        Serial.print(output[1]);
        Serial.print(" motor3 = ");
        Serial.print(output[2]);
        Serial.print(" motor4 = ");
        Serial.println(output[3]);
    }
    
}

void PID_cal() {
    
    //Pitch PID計算
    Error = angle_pitch - pitch_setpoint;
    pid_i_mem_pitch += IpitchConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    
    if(pid_i_mem_pitch > pid_max_pitch)
        pid_i_mem_pitch = pid_max_pitch;
    else if(pid_i_mem_pitch < pid_max_pitch * -1)
        pid_i_mem_pitch = pid_max_pitch * -1;
    
    PIDPitch = PpitchConst * Error + pid_i_mem_pitch + DpitchConst * (Error - LastDPitchError);
    
    //pid 修正 pitch:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    
    if(PIDPitch > pid_max_pitch)
        PIDPitch = pid_max_pitch;
    else if(PIDPitch < pid_max_pitch * -1)
        PIDPitch = pid_max_pitch * -1;
    
    PIDPitch=PIDPitch*0.5;
    
    LastDPitchError = Error;
    
    
    //Roll PID計算
    Error =angle_roll - roll_setpoint;
    pid_i_mem_roll += IrollConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    if(pid_i_mem_roll > pid_max_roll)
        pid_i_mem_roll = pid_max_roll;
    else if(pid_i_mem_roll < pid_max_roll * -1)
        pid_i_mem_roll = pid_max_roll * -1;
    
    PIDRoll = ProllConst * Error + pid_i_mem_roll + DrollConst * (Error -LastDRollError);
    //pid 修正 roll:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    
    if(PIDRoll > pid_max_roll)
        PIDRoll = pid_max_roll;
    else if(PIDRoll < pid_max_roll * -1)
        PIDRoll = pid_max_roll * -1;
    
    PIDRoll=PIDRoll*0.5;
    
    LastDRollError = Error;
    
    
    //Yaw PID計算
    Error = angle_yaw - yaw_setpoint;
    pid_i_mem_yaw += IyawConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    if(pid_i_mem_yaw > pid_max_yaw)
        pid_i_mem_yaw = pid_max_yaw;
    else if(pid_i_mem_yaw < pid_max_yaw * -1)
        pid_i_mem_yaw = pid_max_yaw * -1;
    
    PIDYaw = PyawConst * Error + pid_i_mem_yaw + DyawConst * (Error - LastDYawError);
    //pid 修正 yaw:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    
    if(PIDYaw > pid_max_yaw)
        PIDYaw = pid_max_yaw;
    else if(PIDYaw < pid_max_yaw * -1)
        PIDYaw = pid_max_yaw * -1;
    
    PIDYaw=PIDYaw*0.5;
    
    LastDYawError = Error;
}

void read_mpu_6050_data(){                                             //Subroutine for reading the raw gyro and accelerometer data
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x3B);                                                    //Send the requested starting register
    Wire.endTransmission();                                              //End the transmission
    Wire.requestFrom(0x68,14);                                           //Request 14 bytes from the MPU-6050
    while(Wire.available() < 14);                                        //Wait until all the bytes are received
    acc_x = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_x variable
    acc_y = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_y variable
    acc_z = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_z variable
    temperature = Wire.read()<<8|Wire.read();                            //Add the low and high byte to the temperature variable
    gyro_x = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_x variable
    gyro_y = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_y variable
    gyro_z = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_z variable
    
}

void setup_mpu_6050_registers(){
    //Activate the MPU-6050
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x6B);                                                    //Send the requested starting register
    Wire.write(0x00);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
    //Configure the accelerometer (+/-8g)
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x1C);                                                    //Send the requested starting register
    Wire.write(0x10);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
    //Configure the gyro (500dps full scale)
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x1B);                                                    //Send the requested starting register
    Wire.write(0x08);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
}



void gyro_cal() {
    gyro_x -= gyro_x_cal;             //Subtract the offset calibration value from the raw gyro_x value
    gyro_y -= gyro_y_cal;             //Subtract the offset calibration value from the raw gyro_y value
    gyro_z -= gyro_z_cal;             //Subtract the offset calibration value from the raw gyro_z value
    //Serial.print( gyro_x_cal);Serial.print( gyro_y_cal);Serial.print( gyro_z_cal);delay(1000);
    
    //Gyro angle calculations
    //0.0000611 = 1 / (250Hz / 65.5)
    angle_pitch += gyro_x * time_diff/gyro_sensi;   //Calculate the traveled pitch angle and add this to the angle_pitch variable
    angle_roll += gyro_y * time_diff/gyro_sensi;    //Calculate the traveled roll angle and add this to the angle_roll variable
    angle_yaw += gyro_z * time_diff/gyro_sensi;    //Calculate the traveled roll angle and add this to the angle_roll variable
    
    //0.000001066 = 0.0000611 * (3.142(PI) / 180degr) The Arduino sin function is in radians
    tmp=angle_pitch;
    angle_pitch += angle_roll * sin(gyro_z * time_diff*0.0175/gyro_sensi);               //If the IMU has yawed transfer the roll angle to the pitch angel
    angle_roll -= tmp * sin(gyro_z * time_diff*0.0175/gyro_sensi);               //If the IMU has yawed transfer the pitch angle to the roll angel
    
    /*
     //Accelerometer angle calculations
     acc_total_vector = sqrt((acc_x*acc_x)+(acc_y*acc_y)+(acc_z*acc_z));  //Calculate the total accelerometer vector
     //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
     angle_pitch_acc = asin((float)acc_y/acc_total_vector)* 57.296;       //Calculate the pitch angle
     angle_roll_acc = asin((float)acc_x/acc_total_vector)* -57.296;       //Calculate the roll angle
     
     //Place the MPU-6050 spirit level and note the values in the following two lines for calibration
     angle_pitch_acc -= angle_pitch_cal;             //Accelerometer calibration value for pitch
     angle_roll_acc -= angle_roll_cal;              //Accelerometer calibration value for roll
     
     
     
     if(reset_gyro==101){                                                 //If the IMU is already started
     angle_pitch = angle_pitch * 0.9996 + angle_pitch_acc * 0.0004;     //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
     angle_roll = angle_roll * 0.9996 + angle_roll_acc * 0.0004;        //Correct the drift of the gyro roll angle with the accelerometer roll angle
     reset_gyro=0;
     }
     else {
     reset_gyro++;
     }
     */
    
    
    
    //Accelerometer angle calculations
    acc_total_vector = sqrt((acc_x*acc_x)+(acc_y*acc_y)+(acc_z*acc_z));  //Calculate the total accelerometer vector
    //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
    angle_pitch_acc = asin((float)acc_y/acc_total_vector)* 57.296;       //Calculate the pitch angle
    angle_roll_acc = asin((float)acc_x/acc_total_vector)* -57.296;       //Calculate the roll angle
    
    //Place the MPU-6050 spirit level and note the values in the following two lines for calibration
    angle_pitch_acc -= angle_pitch_cal;             //Accelerometer calibration value for pitch
    angle_roll_acc -=angle_roll_cal;              //Accelerometer calibration value for roll
    angle_yaw_acc -=angle_yaw_cal;              //Accelerometer calibration value for yaw
    
    angle_pitch = angle_pitch * 0.96 + angle_pitch_acc * 0.04;     //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
    angle_roll = angle_roll * 0.96 + angle_roll_acc * 0.04;  //Correct the drift of the gyro roll angle with the accelerometer roll angle
    //angle_yaw = angle_yaw * 0.96 + angle_yaw_acc * 0.04;  //Correct the drift of the gyro roll angle with the accelerometer roll angle
    
    /*
     if(reset_gyro==3){                                                 //If the IMU is already started
     angle_pitch = angle_pitch * 0.96 + angle_pitch_acc * 0.04;     //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
     angle_roll = angle_roll * 0.96 + angle_roll_acc * 0.04;        //Correct the drift of the gyro roll angle with the accelerometer roll angle
     reset_gyro=0;
     }
     else {
     reset_gyro++;
     }
     
     else{                                                                //At first start
     angle_pitch = angle_pitch_acc;          //Set the gyro pitch angle equal to the accelerometer pitch angle
     angle_roll = angle_roll_acc;            //Set the gyro roll angle equal to the accelerometer roll angle
     set_gyro_angles = true;                 //Set the IMU started flag
     }
     */
    
    
    
    
    Serial.print("Pitch: ");Serial.print(angle_pitch);Serial.print("\t");
    Serial.print("Roll: ");Serial.print(angle_roll);Serial.print("\t");
    Serial.print("Yaw: ");Serial.print(angle_yaw);Serial.print("\t");
    //Serial.print("A_P Acc: ");Serial.print(angle_pitch_acc);Serial.print("\t");
    //Serial.print("A_R Acc: ");Serial.print(angle_roll_acc);Serial.println("\t");
    
    
}


/*
void gyro_cal() {
    
    // Read normalized values
    Vector norm = mpu.readNormalizeGyro();
    
    // Calculate Pitch, Roll and Yaw
    pitch = pitch + norm.YAxis * timeStep;
    roll = roll + norm.XAxis * timeStep;
    yaw = yaw + norm.ZAxis * timeStep;
    
    angle_pitch=pitch;
    angle_roll=roll;
    angle_yaw=yaw;
    
    // Output raw
    Serial.print(" Pitch = ");
    Serial.print(pitch);
    Serial.print(" Roll = ");
    Serial.print(roll);
    Serial.print(" Yaw = ");
    Serial.print(yaw);
    
    
}
*/













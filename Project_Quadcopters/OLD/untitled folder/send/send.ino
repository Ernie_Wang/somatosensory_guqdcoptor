#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "Wire.h"

//MPU6050
float angle_pitch=0, angle_roll=0, angle_yaw=0, time_diff;


//radio variable
float Data[5];
//short throttle , yaw , roll , pitch;

RF24 radio(7,8);

const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

//bottoms
int blackpin=3, redpin=5;



//    Declaring mpu_6050 global variables
int gyro_x, gyro_y, gyro_z;
long acc_x, acc_y, acc_z, acc_total_vector;
int temperature;
long gyro_x_basic, gyro_y_basic, gyro_z_basic, acc_x_tmp, acc_y_tmp, acc_z_tmp, angle_pitch_cal, angle_roll_cal;
long gyro_x_cal, gyro_y_cal, gyro_z_cal, acc_x_cal, acc_y_cal, acc_z_cal, angle_yaw_cal;
long first_timer, second_timer;
//float angle_pitch=0, angle_roll=0, angle_yaw=0, time_diff;
int angle_pitch_buffer, angle_roll_buffer, angle_yaw_buffer;
bool set_gyro_angles;
float angle_roll_acc, angle_pitch_acc, angle_yaw_acc;
const int gyro_sensi=65.5;  //dps
const int acc_sensi=8;     //g
int reset_gyro=0;
float tmp;

float PConst=0;

//fnction name
void read_mpu_6050_data();
void setup_mpu_6050_registers();
void gyro_cal();
void readControl();

void setup(void)
{

    pinMode(4, OUTPUT);
    digitalWrite(4, LOW);
    ////bottom set
    pinMode(redpin, INPUT_PULLUP);
    pinMode(blackpin, INPUT_PULLUP);



    ////MPU6050 set
    //   start set up mpu_6050
    Wire.begin();             //Start I2C as master
    
    pinMode(13, OUTPUT);      //Set output 13 (LED) as output
    setup_mpu_6050_registers();              //Setup the registers of the MPU-6050 (500dfs / +/-8g) and start the gyro
    
    //Serial.print("stop here");
    
    digitalWrite(13, HIGH);      //Set digital output 13 high to indicate startup
    for (int cal_int = 0; cal_int < 2000 ; cal_int ++){                  //Run this code 2000 times
        read_mpu_6050_data();                 //Read the raw acc and gyro data from the MPU-6050
        gyro_x_cal += gyro_x;                 //Add the gyro x-axis offset to the gyro_x_cal variable
        gyro_y_cal += gyro_y;                 //Add the gyro y-axis offset to the gyro_y_cal variable
        gyro_z_cal += gyro_z;                 //Add the gyro z-axis offset to the gyro_z_cal variable
        acc_x_cal+=acc_x;
        acc_y_cal+=acc_y;
        acc_z_cal+=acc_z;
        delay(3);                             //Delay 3us to simulate the 250Hz program loop
    }
    gyro_x_cal /= 2000;                       //Divide the gyro_x_cal variable by 2000 to get the avarage offset
    gyro_y_cal /= 2000;                       //Divide the gyro_y_cal variable by 2000 to get the avarage offset
    gyro_z_cal /= 2000;                       //Divide the gyro_z_cal variable by 2000 to get the avarage offset
    acc_x_cal/= 2000;
    acc_y_cal/= 2000;
    acc_z_cal/= 2000;
    
    acc_total_vector = sqrt((acc_x_cal*acc_x_cal)+(acc_y_cal*acc_y_cal)+(acc_z_cal*acc_z_cal));  //Calculate the total accelerometer vector
    //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
    angle_pitch_cal = asin((float)acc_y_cal/acc_total_vector)* 57.296;       //Calculate the pitch angle
    angle_roll_cal = asin((float)acc_x_cal/acc_total_vector)* -57.296;       //Calculate the roll angle
    digitalWrite(13, LOW);                    //All done, turn the LED off
    
    first_timer = micros();                   //Reset the loop timer
    for (int cal_int = 0; cal_int < 2000 ; cal_int ++){                  //Run this code 2000 times
        read_mpu_6050_data();                 //Read the raw acc and gyro data from the MPU-6050
        gyro_x_cal += gyro_x;                 //Add the gyro x-axis offset to the gyro_x_cal variable
        gyro_y_cal += gyro_y;                 //Add the gyro y-axis offset to the gyro_y_cal variable
        gyro_z_cal += gyro_z;                 //Add the gyro z-axis offset to the gyro_z_cal variable
        acc_x_cal+=acc_x;
        acc_y_cal+=acc_y;
        acc_z_cal+=acc_z;
        delay(3);                             //Delay 3us to simulate the 250Hz program loop
    }
    gyro_x_cal /= 2000;                       //Divide the gyro_x_cal variable by 2000 to get the avarage offset
    gyro_y_cal /= 2000;                       //Divide the gyro_y_cal variable by 2000 to get the avarage offset
    gyro_z_cal /= 2000;                       //Divide the gyro_z_cal variable by 2000 to get the avarage offset
    acc_x_cal/= 2000;
    acc_y_cal/= 2000;
    acc_z_cal/= 2000;
    
    acc_total_vector = sqrt((acc_x_cal*acc_x_cal)+(acc_y_cal*acc_y_cal)+(acc_z_cal*acc_z_cal));  //Calculate the total accelerometer vector
    //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
    angle_pitch_cal = asin((float)acc_y_cal/acc_total_vector)* 57.296;       //Calculate the pitch angle
    angle_roll_cal = asin((float)acc_x_cal/acc_total_vector)* -57.296;       //Calculate the roll angle
    digitalWrite(13, LOW);                    //All done, turn the LED off
    
    first_timer = micros();                   //Reset the loop timer
    
    digitalWrite(13, LOW);                    //All done, turn the LED off
    
  
  //set radio
  Serial.begin(57600);
  radio.begin();
  radio.setRetries(15,15);
  
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.stopListening();
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_1MBPS);
  Serial.println("ready");
 // delay(5000);
}

void loop(void)
{
  radio.stopListening();
  read_mpu_6050_data();             //Read the raw acc and gyro data from the MPU-6050
    second_timer=micros();
    time_diff=(second_timer-first_timer)*0.000001;
    first_timer =micros();
    
    //get data
    gyro_cal();

  readControl();
  radio.write(&Data , sizeof(Data));
  Serial.print("sending");
  Serial.print(Data[0]);Serial.print("  ");
  Serial.print(Data[1]);Serial.print("  ");
  Serial.print(Data[2]);Serial.print("  ");
  Serial.print(Data[3]);Serial.println("  ");

  if(Data[0]==0&&Data[1]==0) {
  int count=0;
  while(count<=1) {
    radio.startListening();
  radio.read(&PConst, sizeof(PConst));
  Serial.print("PConst:");Serial.println(PConst);
  count++;
  }
  }
    /*unsigned long time = millis();
    Serial.print("Now sending");
    Serial.print(time);
    bool ok = radio.write( &time, sizeof(unsigned long) );
    if (ok)
      Serial.println(".......OK");
    else
      Serial.println(".......FAILED");*/
//delay(4);
}
void readControl()
{  
  Data[0]=digitalRead(redpin);
    Data[1]=digitalRead(blackpin);
    Data[2]=angle_pitch;
    Data[3]=angle_roll;
}

void read_mpu_6050_data() {                                             //Subroutine for reading the raw gyro and accelerometer data
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x3B);                                                    //Send the requested starting register
    Wire.endTransmission();                                              //End the transmission
    Wire.requestFrom(0x68,14);                                           //Request 14 bytes from the MPU-6050
    while(Wire.available() < 14);                                        //Wait until all the bytes are received
    acc_x = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_x variable
    acc_y = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_y variable
    acc_z = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_z variable
    temperature = Wire.read()<<8|Wire.read();                            //Add the low and high byte to the temperature variable
    gyro_x = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_x variable
    gyro_y = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_y variable
    gyro_z = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_z variable
    
}

void setup_mpu_6050_registers() {
    //Activate the MPU-6050
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x6B);                                                    //Send the requested starting register
    Wire.write(0x00);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
    //Configure the accelerometer (+/-8g)
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x1C);                                                    //Send the requested starting register
    Wire.write(0x10);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
    //Configure the gyro (500dps full scale)
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x1B);                                                    //Send the requested starting register
    Wire.write(0x08);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
}


void gyro_cal() {
    gyro_x -= gyro_x_cal;             //Subtract the offset calibration value from the raw gyro_x value
    gyro_y -= gyro_y_cal;             //Subtract the offset calibration value from the raw gyro_y value
    gyro_z -= gyro_z_cal;             //Subtract the offset calibration value from the raw gyro_z value
    //Serial.print( gyro_x_cal);Serial.print( gyro_y_cal);Serial.print( gyro_z_cal);delay(1000);
    
    //Gyro angle calculations
    //0.0000611 = 1 / (250Hz / 65.5)
    angle_pitch += gyro_x * time_diff/gyro_sensi;   //Calculate the traveled pitch angle and add this to the angle_pitch variable
    angle_roll += gyro_y * time_diff/gyro_sensi;    //Calculate the traveled roll angle and add this to the angle_roll variable
    angle_yaw += gyro_z * time_diff/gyro_sensi;    //Calculate the traveled roll angle and add this to the angle_roll variable
    
    //0.000001066 = 0.0000611 * (3.142(PI) / 180degr) The Arduino sin function is in radians
    tmp=angle_pitch;
    angle_pitch += angle_roll * sin(gyro_z * time_diff*0.0175/gyro_sensi);               //If the IMU has yawed transfer the roll angle to the pitch angel
    angle_roll -= tmp * sin(gyro_z * time_diff*0.0175/gyro_sensi);               //If the IMU has yawed transfer the pitch angle to the roll angel
    
    /*
     //Accelerometer angle calculations
     acc_total_vector = sqrt((acc_x*acc_x)+(acc_y*acc_y)+(acc_z*acc_z));  //Calculate the total accelerometer vector
     //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
     angle_pitch_acc = asin((float)acc_y/acc_total_vector)* 57.296;       //Calculate the pitch angle
     angle_roll_acc = asin((float)acc_x/acc_total_vector)* -57.296;       //Calculate the roll angle
     
     //Place the MPU-6050 spirit level and note the values in the following two lines for calibration
     angle_pitch_acc -= angle_pitch_cal;             //Accelerometer calibration value for pitch
     angle_roll_acc -= angle_roll_cal;              //Accelerometer calibration value for roll
     
     
     
     if(reset_gyro==101){                                                 //If the IMU is already started
     angle_pitch = angle_pitch * 0.9996 + angle_pitch_acc * 0.0004;     //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
     angle_roll = angle_roll * 0.9996 + angle_roll_acc * 0.0004;        //Correct the drift of the gyro roll angle with the accelerometer roll angle
     reset_gyro=0;
     }
     else {
     reset_gyro++;
     }
     */
    
    
    
    //Accelerometer angle calculations
    acc_total_vector = sqrt((acc_x*acc_x)+(acc_y*acc_y)+(acc_z*acc_z));  //Calculate the total accelerometer vector
    //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
    angle_pitch_acc = asin((float)acc_y/acc_total_vector)* 57.296;       //Calculate the pitch angle
    angle_roll_acc = asin((float)acc_x/acc_total_vector)* -57.296;       //Calculate the roll angle
    
    //Place the MPU-6050 spirit level and note the values in the following two lines for calibration
    angle_pitch_acc -= angle_pitch_cal;             //Accelerometer calibration value for pitch
    angle_roll_acc -=angle_roll_cal;              //Accelerometer calibration value for roll
    angle_yaw_acc -=angle_yaw_cal;              //Accelerometer calibration value for yaw
    
    angle_pitch = angle_pitch * 0.96 + angle_pitch_acc * 0.04;     //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
    angle_roll = angle_roll * 0.96 + angle_roll_acc * 0.04;  //Correct the drift of the gyro roll angle with the accelerometer roll angle
    angle_yaw = angle_yaw * 0.96 + angle_yaw_acc * 0.04;  //Correct the drift of the gyro roll angle with the accelerometer roll angle
    
    /*
     if(reset_gyro==3){                                                 //If the IMU is already started
     angle_pitch = angle_pitch * 0.96 + angle_pitch_acc * 0.04;     //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
     angle_roll = angle_roll * 0.96 + angle_roll_acc * 0.04;        //Correct the drift of the gyro roll angle with the accelerometer roll angle
     reset_gyro=0;
     }
     else {
     reset_gyro++;
     }
     
     else{                                                                //At first start
     angle_pitch = angle_pitch_acc;          //Set the gyro pitch angle equal to the accelerometer pitch angle
     angle_roll = angle_roll_acc;            //Set the gyro roll angle equal to the accelerometer roll angle
     set_gyro_angles = true;                 //Set the IMU started flag
     }
     */
    
    
    
    
    Serial.print("Pitch: ");Serial.print(angle_pitch);Serial.print("\t");
    Serial.print("Roll: ");Serial.print(angle_roll);Serial.print("\t");
    Serial.print("Yaw: ");Serial.print(angle_yaw);Serial.print("\t");
    //Serial.print("A_P Acc: ");Serial.print(angle_pitch_acc);Serial.print("\t");
    //Serial.print("A_R Acc: ");Serial.print(angle_roll_acc);Serial.println("\t");
}

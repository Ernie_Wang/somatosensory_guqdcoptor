
#include <SPI.h>
#include "RF24.h"

//  RF
#define CE_pin 7
#define CSN_pin 8
#define SCK_pin 13
#define MISO_pin 12
#define MOSI_pin 11
//#define SS_pin 10

#define Channel 30  //頻道 0~127

//bool radioNumber = 1; // 0為接收 1為輸出
byte addresses[][6] = {"1Node","2Node"};  //通關暗號

RF24 radio(CE_pin,CSN_pin);

void setup() 
{ 
  Serial.begin(57600);
  Serial.println("RF control");
  radio.begin();
pinMode(3,OUTPUT);
  digitalWrite(3, LOW);
  radio.setPALevel(RF24_PA_MIN);    //功率放大器，可選RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX, RF24_PA_ERROR
  radio.setDataRate(RF24_2MBPS);  //傳輸速率，可選RF24_1MBPS, RF24_2MBPS, RF24_250KBPS
  radio.setAutoAck(false);          //自動回應，預設為true
  
  radio.setChannel(Channel);
  radio.openWritingPipe(addresses[1]);
  radio.openReadingPipe(1,addresses[0]);
  
 // radio.stopListening();
  
} 

void loop() 
{
  
  //  RF傳輸
  unsigned long TX_inf=0;
  
  
  Serial.print("Now sending inf: ");
  Serial.println(TX_inf);
  
  if (!radio.write(&TX_inf, sizeof(unsigned long) )){
    Serial.println("failed");
  }
        
  delay(500);
}

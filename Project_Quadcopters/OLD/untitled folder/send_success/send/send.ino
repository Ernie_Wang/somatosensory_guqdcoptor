#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

//MPU6050
float angle_pitch=0, angle_roll=0, angle_yaw=0, time_diff;

float PConst;

//radio variable
float Data[5];
//short throttle , yaw , roll , pitch;

RF24 radio(7,8);

//bottoms
int blackpin=3, redpin=5;

const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

void setup(void)
{

    pinMode(4, OUTPUT);
    digitalWrite(4, LOW);
    ////bottom set
    pinMode(redpin, INPUT_PULLUP);
    pinMode(blackpin, INPUT_PULLUP);

  
  //set radio
  Serial.begin(57600);
  radio.begin();
  radio.setRetries(15,15);
  
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.stopListening();
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_1MBPS);
  Serial.println("ready");
 // delay(5000);
}

void loop(void)
{
  radio.stopListening();
  readControl();
  radio.write(&Data , sizeof(Data));
  Serial.print("sending");
  Serial.print(Data[0]);Serial.print("  ");
  Serial.print(Data[1]);Serial.print("  ");
  Serial.print(Data[2]);Serial.print("  ");
  Serial.print(Data[3]);Serial.println("  ");
  if(Data[0]==0&&Data[1]==0) {
      int count=0;
      while(count<=0) {
        radio.startListening();
        radio.read(&PConst, sizeof(PConst));
        Serial.print("PConst:");Serial.println(PConst);
        count++;
      }
  }
    /*unsigned long time = millis();
    Serial.print("Now sending");
    Serial.print(time);
    bool ok = radio.write( &time, sizeof(unsigned long) );
    if (ok)
      Serial.println(".......OK");
    else
      Serial.println(".......FAILED");*/
//delay(4);
}
void readControl()
{  
  Data[0]=digitalRead(redpin);
    Data[1]=digitalRead(blackpin);
    Data[2]=angle_pitch;
    Data[3]=angle_roll;
}



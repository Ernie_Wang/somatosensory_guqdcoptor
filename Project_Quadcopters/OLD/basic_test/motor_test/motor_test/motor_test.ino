//
// motor_test
//
// Description of the project
// Developed with [embedXcode](http://embedXcode.weebly.com)
//
// Author 		王昶文
// 				王昶文
//
// Date			2017/2/4 16:44
// Version		<#version#>
//
// Copyright	© 王昶文, 2017年
// Licence		<#licence#>
//
// See         ReadMe.txt for references
//


// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

// Set parameters


// Include application, user and local libraries


// Define structures and classes


// Define variables and constants


// Prototypes


// Utilities


// Functions


// Add setup code
void setup()
{
    pinMode(3, OUTPUT);
    Serial.begin(9600);
}

// Add loop code
int val=0;
void loop()
{
    if (Serial.available()) {
        val+=Serial.read();
        if (val>=0&&val<=255) {
            analogWrite(3, val);
            Serial.print("Throttle : ");
            Serial.println(val);
        }
    }
}


















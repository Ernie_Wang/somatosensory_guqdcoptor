// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE





////---------------MY PROGRAM START FROM HERE-------------------////







//#include "Wire.h"
const int motor[] = {3, 6, 9, 11};// 1=>3  2=>6  3=>9  4=>11
bool controled=false, start=false;
int throttle=0;
int countstart=0, countend=0, countdiff=0;
bool throttlezero=false;
bool starting=false;

//    Declaring mpu_6050 global variables
int gyro_x, gyro_y, gyro_z;//read value
long acc_x, acc_y, acc_z, acc_total_vector;//read value
long gyro_x_basic, gyro_y_basic, gyro_z_basic, acc_x_tmp, acc_y_tmp, acc_z_tmp, angle_pitch_cal, angle_roll_cal;
long first_timer, second_timer, time_diff; //calaulate the time pass
float angle_pitch, angle_roll, angle_yaw ; //the angle of the quadcoptors now
float angle_roll_acc, angle_pitch_acc;
const int gyro_sensi=65.5;  //dps
float tmp; //for MPU_6050


//    PID global valiables
float roll_setpoint =0, pitch_setpoint =0; //設定PID基準點
float Error;
int front[]={1, 4}, back[]={2, 3}, right[]={3, 4}, left[]={1, 2};




void read_mpu_6050_data();
void setup_mpu_6050_registers();
void gyro_cal();
void PID_cal();

// Add setup code
void setup()
{
    //   start set up mpu_6050
    Wire.begin();             //Start I2C as master
    Serial.begin(57600);      //Use only for debugging
    pinMode(13, OUTPUT);      //Set output 13 (LED) as output
    
    setup_mpu_6050_registers();              //Setup the registers of the MPU-6050 (500dfs / +/-8g) and start the gyro
    
    digitalWrite(13, HIGH);      //Set digital output 13 high to indicate startup
    for (int cal_int = 0; cal_int < 2000 ; cal_int ++){ //Run this code 2000 times
        read_mpu_6050_data();    //Read the raw acc and gyro data from the MPU-6050
        gyro_x_basic += gyro_x;    //Add the gyro x-axis offset to the gyro_x_cal variable
        gyro_y_basic += gyro_y;    //Add the gyro y-axis offset to the gyro_y_cal variable
        gyro_z_basic += gyro_z;    //Add the gyro z-axis offset to the gyro_z_cal variable
        acc_x_tmp+=acc_x;
        acc_y_tmp+=acc_y;
        acc_z_tmp+=acc_z;
        delay(3);                //Delay 3us to simulate the 250Hz program loop
    }
    gyro_x_basic / 2000;                       //Divide the gyro_x_cal variable by 2000 to get the avarage offset
    gyro_y_basic / 2000;                       //Divide the gyro_y_cal variable by 2000 to get the avarage offset
    gyro_z_basic / 2000;                       //Divide the gyro_z_cal variable by 2000 to get the avarage offset
    acc_x_tmp/ 2000;
    acc_y_tmp/ 2000;
    acc_z_tmp/ 2000;
    acc_total_vector = sqrt((acc_x_tmp*acc_x_tmp)+(acc_y_tmp*acc_y_tmp)+(acc_z_tmp*acc_z_tmp));  //Calculate the total accelerometer vector
    //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
    angle_pitch_cal = asin((float)acc_y_tmp/acc_total_vector)* 57.296;       //Calculate the pitch angle
    angle_roll_cal = asin((float)acc_x_tmp/acc_total_vector)* -57.296;       //Calculate the roll angle
    
    digitalWrite(13, LOW);                    //All done, turn the LED off
    
    

}

// Add loop code
void loop()
{
    starting=false;
    if (throttle==0) {
        if (      /* the countrol buttom*2 */) {    //tell the coptors to start
            
            if (throttlezero==false) {        //judge whether *countstart  *countend is counting for the shut down program
                if (countstart==0) {
                    countstart=millis();      //record the time of countstart;
                }
                countend=millis();            //record the time of countend;
                countdiff=countend-countstart;
                if (countdiff>=2500) {        //when the difference between countstart and countend is 2.5 second
                    start=true;               //you can start to control coptors
                    countstart=0;
                    countend=0;
                    throttle=1;
                    first_timer = micros();                   //Reset the loop timer
                }
            }
            else {                            //if the countstart and countend is counting for the shut down, reset to zero
                throttlezero=false;
                countstart=0;
                countend=0;
            }
            starting=true;
        }
    }
    
    
    if (start) {     //if haven't start, you won't able to start to control
        
        
        
        //remote first, balance second, if go into remote loop, skip the balance
        
        //remote
        if (   true   /*  reciving the signal  */) {
            if (    throttle<=253/*  throttle up  */) {
                throttle+=2;
                //Serial.print("throttle : ");
                //Serial.println(throttle);
                throttlezero=false;
                for (int i=0; i<4; i++) {
                    analogWrite(motor[i], throttle);
                }
                
                delay(10);
            }
            if (    throttle>=3/*  throttle down  */) {
                throttle-=2;
                //Serial.print("throttle : ");
                //Serial.println(throttle);
                throttlezero=false;
                for (int i=0; i<4; i++) {
                    analogWrite(motor[i], throttle);
                }
                delay(10);
            }
            if (    /*  controller move up  */) {
                <#statements#>
            }
            if (    /*  controller move down  */) {
                <#statements#>
            }
            if (    /*  controller move right  */) {
                <#statements#>
            }
            if (    /*  controller move left  */) {
                <#statements#>
            }
            
            
            
            throttlezero=false;
            first_timer = micros();                   //Reset the loop timer
        }
        else {
            roll_setpoint =0;
            pitch_setpoint =0;
        }
        
        
        //balance //PID
        if (throttle>3) {
            
                
                gyro_cal();
                
                
                
                
                
            
            
            
        }
        
        
    }
    
    
    if (throttle<=3) {     //shut down the coptors for safety   //the logic is same as start
        if (start) {
            if (!starting) {
                if (throttlezero==true) {
                    if (countstart==0) {
                        countstart=millis();
                    }
                    countend=millis();
                    countdiff=countend-countstart;
                    if (countdiff>=5000) {
                        start=false;
                        countstart=0;
                        countend=0;
                        throttle=0;
                        Serial.println("Engine down ");
                        Serial.print("throttle : ");
                        Serial.println(throttle);
                    }
                } else {
                    throttlezero=true;
                    countstart=0;
                    countend=0;
                }
            }
        }
        first_timer = micros();                   //Reset the loop timer
    }
    
}


void read_mpu_6050_data() {                                             //Subroutine for reading the raw gyro and accelerometer data
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x3B);                                                    //Send the requested starting register
    Wire.endTransmission();                                              //End the transmission
    Wire.requestFrom(0x68,14);                                           //Request 14 bytes from the MPU-6050
    while(Wire.available() < 14);                                        //Wait until all the bytes are received
    acc_x = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_x variable
    acc_y = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_y variable
    acc_z = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_z variable
    temperature = Wire.read()<<8|Wire.read();                            //Add the low and high byte to the temperature variable
    gyro_x = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_x variable
    gyro_y = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_y variable
    gyro_z = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_z variable
    
}

void setup_mpu_6050_registers() {
    //Activate the MPU-6050
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x6B);                                                    //Send the requested starting register
    Wire.write(0x00);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
    //Configure the accelerometer (+/-8g)
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x1C);                                                    //Send the requested starting register
    Wire.write(0x10);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
    //Configure the gyro (500dps full scale)
    Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
    Wire.write(0x1B);                                                    //Send the requested starting register
    Wire.write(0x08);                                                    //Set the requested starting register
    Wire.endTransmission();                                              //End the transmission
}

void gyro_cal() {
    //mpu6050//
    read_mpu_6050_data();             //Read the raw acc and gyro data from the MPU-6050
    
    
    second_timer=micros();
    
    time_diff=second_timer-first_timer;
    time_diff=time_diff/1000000;
    
    first_timer=second_timer;
    
    
    
    gyro_x -= gyro_x_basic;         //Subtract the offset calibration value from the raw gyro_x value
    gyro_y -= gyro_y_basic;         //Subtract the offset calibration value from the raw gyro_y value
    gyro_z -= gyro_z_basic;         //Subtract the offset calibration value from the raw gyro_z value
    //Serial.print( gyro_x_cal);Serial.print( gyro_y_cal);Serial.print( gyro_z_cal);delay(1000);
    
    //Gyro angle calculations
    angle_pitch += gyro_x * time_diff/gyro_sensi;   //Calculate the traveled pitch angle and add this to the angle_pitch variable
    angle_roll += gyro_y * time_diff/gyro_sensi;    //Calculate the traveled roll angle and add this to the angle_roll variable
    angle_yaw += gyro_z * time_diff/gyro_sensi;    //Calculate the traveled roll angle and add this to the angle_roll variable
    
    tmp=angle_pitch;
    angle_pitch += angle_roll * sin(gyro_z * time_diff*0.0175/gyro_sensi);//If the IMU has yawed transfer the roll angle to the pitch angel
    angle_roll -= tmp * sin(gyro_z * time_diff*0.0175/gyro_sensi);//If the IMU has yawed transfer the pitch angle to the roll angel
    
    
    //Accelerometer angle calculations
    acc_total_vector = sqrt((acc_x*acc_x)+(acc_y*acc_y)+(acc_z*acc_z));  //Calculate the total accelerometer vector
    //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
    angle_pitch_acc = asin((float)acc_y/acc_total_vector)* 57.296;//Calculate the pitch angle
    angle_roll_acc = asin((float)acc_x/acc_total_vector)* -57.296;//Calculate the roll angle
    
    //Place the MPU-6050 spirit level and note the values in the following two lines for calibration
    angle_pitch_acc -= angle_pitch_cal;             //Accelerometer calibration value for pitch
    angle_roll_acc -=angle_roll_cal;              //Accelerometer calibration value for roll
    
    angle_pitch = angle_pitch * 0.96 + angle_pitch_acc * 0.04;     //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
    angle_roll = angle_roll * 0.96 + angle_roll_acc * 0.04;        //Correct the drift of the gyro roll angle with the accelerometer roll angle
    
    
    
    
    //Serial.print("Pitch: ");Serial.print(angle_pitch);Serial.print("\t");
    //Serial.print("Roll: ");Serial.print(angle_roll);Serial.print("\t");
    //Serial.print("Yaw: ");Serial.print(angle_yaw);Serial.print("\t");
    //Serial.print("A_P Acc: ");Serial.print(angle_pitch_acc);Serial.print("\t");
    //Serial.print("A_R Acc: ");Serial.print(angle_roll_acc);Serial.println("\t");
    

}

void PID_cal() {
    
    //Pitch PID計算
    Error = angle_pitch - pitch_setpoint;
    pid_i_mem_pitch += IpitchConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    if(pid_i_mem_pitch > pid_max_pitch)
        pid_i_mem_pitch = pid_max_pitch;
    else if(pid_i_mem_pitch < pid_max_pitch * -1)
        pid_i_mem_pitch = pid_max_pitch * -1;
    
    PIDPitch = PpitchConst * Error + pid_i_mem_pitch + DpitchConst * (Error - LastDPitchError);
    //pid 修正 pitch:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    if(PIDPitch > pid_max_pitch)
        PIDPitch = pid_max_pitch;
    else if(PIDPitch < pid_max_pitch * -1)
        PIDPitch = pid_max_pitch * -1;
    
    LastDPitchError = Error;
    
    
    //Roll PID計算
    Error =angle_roll - roll_setpoint;
    pid_i_mem_roll += IrollConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    if(pid_i_mem_roll > pid_max_roll)
        pid_i_mem_roll = pid_max_roll;
    else if(pid_i_mem_roll < pid_max_roll * -1)
        pid_i_mem_roll = pid_max_roll * -1;
    
    PIDRoll = ProllConst * Error + pid_i_mem_roll + DrollConst * (Error -LastDRollError);
    //pid 修正 roll:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    if(PIDRoll > pid_max_roll)
        PIDRoll = pid_max_roll;
    else if(PIDRoll < pid_max_roll * -1)
        PIDRoll = pid_max_roll * -1;
    
    LastDRollError = Error;
    
    
    //Yaw PID計算
    Error = gyro_yaw_input - yaw_setpoint;
    pid_i_mem_yaw += IyawConst * Error;                    //(PID I累積誤差 I常數*誤差值)
    if(pid_i_mem_yaw > pid_max_yaw)
        pid_i_mem_yaw = pid_max_yaw;
    else if(pid_i_mem_yaw < pid_max_yaw * -1)
        pid_i_mem_yaw = pid_max_yaw * -1;
    
    PIDYaw = PyawConst * Error + pid_i_mem_yaw + DyawConst * (Error - LastDYawError);
    //pid 修正 yaw:        P(P常數*誤差量)             +I(I常數*累積誤差)+ D
    if(PIDYaw > pid_max_yaw)
        PIDYaw = pid_max_yaw;
    else if(PIDYaw < pid_max_yaw * -1)
        PIDYaw = pid_max_yaw * -1;
    
    LastDYawError = Error;
}











